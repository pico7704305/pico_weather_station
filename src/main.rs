#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]

use chrono_light::prelude::Calendar;
use core::cell::RefCell;
use core::fmt::Write;
use core::future::Future;
use core::ops::{Deref, DerefMut};
use core::str;
use core::sync::atomic;
use core::sync::atomic::{AtomicI32, AtomicU16, Ordering};

use bosch_bme680::{Bme680, DeviceAddress, MeasurmentData};
use byteorder::ByteOrder;
use cyw43_pio::PioSpi;
use defmt::{error, info, unwrap, warn};
use defmt_rtt;
use ds323x::{Datelike, DateTimeAccess, NaiveDateTime, NaiveTime, Rtcc, Timelike};
use ds323x::NaiveDate;
use eg_seven_segment::{SevenSegmentStyle, SevenSegmentStyleBuilder};
use embassy_embedded_hal::shared_bus::blocking;
use embassy_executor::main;
use embassy_executor::raw::Executor;
use embassy_executor::Spawner;
use embassy_futures::yield_now;
use embassy_net::{Ipv4Address, Ipv4Cidr, Stack};
use embassy_net_wiznet::Device;
use embassy_rp::{bind_interrupts, Peripheral, PeripheralRef, Peripherals};
use embassy_rp::clocks::RoscRng;
use embassy_rp::gpio::{AnyPin, Input, Level, Output, Pin, Pull};
use embassy_rp::i2c;
use embassy_rp::multicore;
use embassy_rp::peripherals::{DMA_CH0, I2C0, I2C1, PIN_18, PIN_23, PIN_25, PIO0, PWM_CH1, RTC, UART0};
use embassy_rp::pio::Pio;
use embassy_rp::pwm::Pwm;
use embassy_rp::rtc::{DateTime, DayOfWeek, Rtc};
use embassy_rp::spi;
use embassy_rp::uart;
use embassy_sync::blocking_mutex;
use embassy_sync::channel::Channel;
use embassy_sync::mutex;
use embassy_time::{Delay, Duration, Timer};
use embedded_graphics::mono_font::{MonoTextStyle, MonoTextStyleBuilder};
use embedded_graphics::mono_font::ascii::{FONT_10X20, FONT_6X9};
use embedded_graphics::pixelcolor::Rgb565;
use embedded_graphics::prelude::*;
use embedded_graphics::primitives::PrimitiveStyle;
use embedded_graphics::text::{Alignment, Baseline, Text, TextStyle, TextStyleBuilder};
use embedded_hal::i2c::I2c;
use epd_waveshare::{epd2in9_v2::*, prelude::*};
use heapless::String;
use micromath::F32Ext;
use panic_probe;
use profont::{PROFONT_18_POINT, PROFONT_24_POINT};
use rand::RngCore;
use serde::{Deserialize, Serialize};
use serde_json_core;
use static_cell::{make_static, StaticCell};

use crate::mqtt_client::{MQTTClient, MqttSubscribeData};
use crate::ntp_client::NTPClient;
use crate::touch_mapper::TouchPosMapper;
use crate::tp_sensor::TpSensor;

mod mqtt_client;
mod ntp_client;
mod tp_sensor;
mod touch_mapper;

const DISPLAY_FREQ: u32 = 64_000_000;
const WIFI_NETWORK: &str = env!("SSID");
const WIFI_PASSWORD: &str = env!("PASSWORD");
const MQTT_BROKER_IP: &str = env!("MQTT_BROKER_IP");
const MQTT_USER: &str = env!("MQTT_USER");
const MQTT_PASSWD: &str = env!("MQTT_PASSWD");
const MQTT_BROKER_PORT: &str = env!("MQTT_BROKER_PORT");

const DST_DIFF: u64 = 3600;
const UTC_TZ_DIFF: u64 = 3600;

const TIME_DATE_STRING_LENGTH: usize = 19;


#[derive(Serialize, Deserialize, Debug, Clone)]
struct SensorData {
    temperature: f32,
    humidity: f32,
    pressure: f32,
    #[serde(default)]
    valid: bool,
    date: String<TIME_DATE_STRING_LENGTH>,
    #[serde(default)]
    time: i64,
}

impl SensorData {
    pub fn fill_from_bme680(&mut self, data: MeasurmentData, date: &str, dt: i64) {
        self.temperature = self.round(data.temperature);
        self.pressure = self.round(data.pressure / 100.0);
        self.humidity = self.round(data.humidity);
        self.valid = true;
        self.date = String::from(date);
        self.time = dt
    }

    fn round(&mut self, value: f32) -> f32 {
        ((value * 10.0) as i32) as f32 / 10.0
    }

    pub fn as_json_string<const N: usize>(&mut self) -> String<N> {
        serde_json_core::to_string::<SensorData, N>(self).unwrap()
    }
}

impl core::fmt::Display for SensorData {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "{}/{}/{}/{}", self.temperature, self.pressure, self.humidity, self.date)
    }
}

struct CharacterStyles<'a> {
    pub default_character_style: Option<MonoTextStyle<'a, Color>>,
    pub small_character_style: Option<MonoTextStyle<'a, Color>>,
    pub text_style_center_aligned: Option<TextStyle>,
    pub fill_style: Option<PrimitiveStyle<Color>>,
    pub seven_segment_style: Option<SevenSegmentStyle<Color>>,
}

impl<'a> CharacterStyles<'a> {
    pub fn init(&mut self) {
        self.default_character_style = Some(MonoTextStyleBuilder::new()
            .font(&FONT_10X20)
            .text_color(Color::Black)
            .background_color(Color::White)
            .build());
        self.small_character_style = Some(MonoTextStyleBuilder::new()
            .font(&FONT_6X9)
            .text_color(Color::Black)
            .background_color(Color::White)
            .build());
        self.text_style_center_aligned = Some(TextStyleBuilder::new()
            .alignment(Alignment::Left)
            .baseline(Baseline::Middle)
            .build());

        self.fill_style = Some(PrimitiveStyle::with_fill(Color::White));

        let mut segment_font = SevenSegmentStyleBuilder::new()
            .digit_size(Size::new(26, 65))
            .digit_spacing(3)
            .segment_width(7)
            .segment_color(Color::Black)
            .build();
        segment_font.inactive_segment_color = Some(Color::White);

        self.seven_segment_style = Some(segment_font);
    }

    pub fn default_character_style(&mut self) -> MonoTextStyle<'a, Color> {
        self.default_character_style.unwrap()
    }
    pub fn small_character_style(&mut self) -> MonoTextStyle<'a, Color> {
        self.small_character_style.unwrap()
    }

    pub fn text_style_center_aligned(&mut self) -> TextStyle {
        self.text_style_center_aligned.unwrap()
    }

    pub fn seven_segment_style(&mut self) -> SevenSegmentStyle<Color> {
        self.seven_segment_style.unwrap()
    }
    pub fn fill_style(&mut self) -> PrimitiveStyle<Color> {
        self.fill_style.unwrap()
    }
}

static BME688_DATA: mutex::Mutex<blocking_mutex::raw::CriticalSectionRawMutex, RefCell<SensorData>> = mutex::Mutex::new(RefCell::new(SensorData {
    temperature: 0.0,
    humidity: 0.0,
    pressure: 0.0,
    valid: false,
    date: String::new(),
    time: 0,
}));

static mut MQTT_SUBSCRIBE_DATA: MqttSubscribeData = MqttSubscribeData {
    topic: String::new(),
    msg: String::new(),
};

static CHANNEL: Channel<blocking_mutex::raw::CriticalSectionRawMutex, [u16; 4], 1> = Channel::new();

bind_interrupts!(struct Irqs {
    PIO0_IRQ_0 => embassy_rp::pio::InterruptHandler<PIO0>;
    I2C0_IRQ => embassy_rp::i2c::InterruptHandler<I2C0>;
});

#[embassy_executor::task]
async fn wifi_task(
    runner: cyw43::Runner<
        'static,
        Output<'static, PIN_23>,
        PioSpi<'static, PIN_25, PIO0, 0, DMA_CH0>,
    >,
) -> ! {
    runner.run().await
}

#[embassy_executor::task]
async fn net_task(stack: &'static Stack<cyw43::NetDriver<'static>>) -> ! {
    stack.run().await
}

fn from_ascii(bytes: &[u8]) -> Result<&str, &'static str> {
    if bytes.iter().all(|b| *b < 128) {
        Ok(unsafe { core::str::from_utf8_unchecked(bytes) })
    } else {
        Err("Not an ascii!")
    }
}

// #[embassy_executor::task]
// async fn handle_mqtt_publish(stack: &'static embassy_net::Stack<Device<'static>>, rtc: &'static Rtc<'static, RTC>) {
//     let mqtt_tx_buf = &mut *make_static!([0u8; 4096]);
//     let mqtt_rx_buf = &mut *make_static!([0u8; 4096]);
//     let mut mqttclient = mqtt_client::MQTTClient::new(stack, mqtt_rx_buf, mqtt_tx_buf);
//
//     loop {
//         let mut payload: String<256> = BME688_DATA.lock().await.get_mut().as_json_string();
//         let dt = rtc.now().unwrap();
//
//         info!("payload = {}", payload.as_str());
//
//         if let Err(e) = mqttclient.connect("pico_rust_publish", MQTT_BROKER_IP, MQTT_BROKER_PORT.parse().unwrap(), MQTT_USER, MQTT_PASSWD).await {
//             warn!("connect error {}", e);
//             continue;
//         }
//         if let Err(e) = mqttclient.publish("pico/weather/indoor", payload.as_str(), true, 1).await {
//             warn!("publish error {}", e);
//         }
//         mqttclient.disconnect().await;
//         CHANNEL.send([5, 0, 0, 0]).await;
//
//         // 120s
//         Timer::after(Duration::from_secs(((60 - dt.second) + 60) as u64)).await;
//     }
// }

#[embassy_executor::task]
async fn handle_mqtt_subscribe_periodic(stack: &'static embassy_net::Stack<Device<'static>>, rtc: &'static Rtc<'static, RTC>) {
    let mqtt_tx_buf = &mut *make_static!([0u8; 4096]);
    let mqtt_rx_buf = &mut *make_static!([0u8; 4096]);
    let mut mqttclient = mqtt_client::MQTTClient::new(stack, mqtt_rx_buf, mqtt_tx_buf);

    loop {
        let dt = rtc.now().unwrap();

        info!("subscribe_periodic");

        if let Err(e) = mqttclient.connect("pico_rust_subscribe", MQTT_BROKER_IP, MQTT_BROKER_PORT.parse().unwrap(), MQTT_USER, MQTT_PASSWD).await {
            warn!("connect error {}", e);
            continue;
        }
        if let Err(e) = mqttclient.subscribe_oneshot("pico/weather/outdoor", 1, mqtt_subscribe_cb).await {
            warn!("subscribe error {}", e);
        }

        // 120s
        Timer::after(Duration::from_secs(((60 - dt.second) + 60) as u64)).await;
    }
}

async fn mqtt_subscribe_cb(data: MqttSubscribeData) -> bool {
    unsafe {
        MQTT_SUBSCRIBE_DATA = data;
        info!("mqtt_subscribe_cb {}", MQTT_SUBSCRIBE_DATA);
    }
    CHANNEL.send([5, 0, 0, 0]).await;
    true
}

// #[embassy_executor::task]
// async fn handle_mqtt_subscribe(stack: &'static embassy_net::Stack<Device<'static>>) {
//     let mqtt_tx_buf = &mut *make_static!([0u8; 4096]);
//     let mqtt_rx_buf = &mut *make_static!([0u8; 4096]);
//     let mut mqttclient = mqtt_client::MQTTClient::new(stack, mqtt_rx_buf, mqtt_tx_buf);
//     // let mut subscribe_count: u16 = 0;
//     loop {
//         match mqttclient.connect("pico_rust_subscribe", MQTT_BROKER_IP, MQTT_BROKER_PORT.parse().unwrap(), MQTT_USER, MQTT_PASSWD).await {
//             Ok(_) => {
//                 // subscribe_count += 1;
//                 // SUBSCRIBE_COUNT.store(subscribe_count, atomic::Ordering::Relaxed);
//                 if let Err(e) = mqttclient.subscribe("pico/weather/outdoor", 1, mqtt_subscribe_cb).await {
//                     warn!("subscribe error {}", e);
//                 }
//                 mqttclient.disconnect().await;
//             }
//
//             Err(e) => {
//                 warn!("connect error {}", e);
//             }
//         }
//     }
// }

#[embassy_executor::task]
async fn handle_tp_sensor(i2c: blocking::i2c::I2cDevice<'static, blocking_mutex::raw::ThreadModeRawMutex, i2c::I2c<'static, I2C0, i2c::Blocking>>,
                          int_pin: AnyPin, rst_pin: AnyPin, size: Size, rotation: DisplayRotation) {
    let mut int = Input::new(int_pin, Pull::Down);
    let mut rst = Output::new(rst_pin, Level::Low);
    // init
    rst.set_high();
    Timer::after(Duration::from_millis(100)).await;
    rst.set_low();
    Timer::after(Duration::from_millis(100)).await;
    rst.set_high();
    Timer::after(Duration::from_millis(100)).await;

    let mut tp_sensor = TpSensor::new(i2c);
    let pos_mapper = TouchPosMapper::new(WIDTH as u16, HEIGHT as u16, (0, WIDTH as u16), (0, HEIGHT as u16));

    let mut handle_touch: bool = false;

    loop {
        int.wait_for_low().await;
        if let Ok((x, y, p)) = tp_sensor.touch() {
            if !handle_touch {
                handle_touch = true;

                let (x, y) = match rotation {
                    DisplayRotation::Rotate0 => pos_mapper.map_touch_pos(x, y, size.width as u16, size.height as u16, 0),
                    DisplayRotation::Rotate90 => pos_mapper.map_touch_pos(x, y, size.width as u16, size.height as u16, 1),
                    DisplayRotation::Rotate180 => pos_mapper.map_touch_pos(x, y, size.width as u16, size.height as u16, 2),
                    DisplayRotation::Rotate270 => pos_mapper.map_touch_pos(x, y, size.width as u16, size.height as u16, 3),
                };
                info!("handle_tp_sensor {} {} {}", x, y, p);
                CHANNEL.send([3, x, y, p]).await;
                Timer::after(Duration::from_millis(250)).await;
            } else {
                handle_touch = false;
            }
        }
    }
}

#[embassy_executor::task]
async fn handle_bme68x_sensor(i2c: blocking::i2c::I2cDevice<'static, blocking_mutex::raw::ThreadModeRawMutex, i2c::I2c<'static, I2C0, i2c::Blocking>>, stack: &'static embassy_net::Stack<Device<'static>>, rtc: &'static Rtc<'static, RTC>) {
    let config = bosch_bme680::Configuration::default();
    let result = Bme680::new(i2c, DeviceAddress::Primary, Delay, &config, 20);
    if result.is_err() {
        error!("Failed setup bme680");
    } else {
        let mut bme = result.unwrap();
        info!("start using bme680");

        let mqtt_tx_buf = &mut *make_static!([0u8; 4096]);
        let mqtt_rx_buf = &mut *make_static!([0u8; 4096]);
        let mut mqttclient = mqtt_client::MQTTClient::new(stack, mqtt_rx_buf, mqtt_tx_buf);

        loop {
            let dt = rtc.now().unwrap();
            let seconds = dt.second as u16;

            match bme.measure() {
                Ok(data) => {
                    let d = NaiveDate::from_ymd_opt(dt.year as i32, dt.month as u32, dt.day as u32).unwrap();
                    let t = NaiveTime::from_hms_milli_opt(dt.hour as u32, dt.minute as u32, dt.second as u32, 0).unwrap();

                    let dt_native = NaiveDateTime::new(d, t);
                    let dt_seconds = dt_native.timestamp();
                    BME688_DATA.lock().await.get_mut().fill_from_bme680(data, get_datetime_formatted(dt).as_str(), dt_seconds);

                    let mut payload: String<256> = BME688_DATA.lock().await.get_mut().as_json_string();

                    info!("payload = {}", payload.as_str());

                    if let Err(e) = mqttclient.connect("pico_rust_publish", MQTT_BROKER_IP, MQTT_BROKER_PORT.parse().unwrap(), MQTT_USER, MQTT_PASSWD).await {
                        warn!("connect error {}", e);
                        continue;
                    }
                    if let Err(e) = mqttclient.publish("pico/weather/indoor", payload.as_str(), true, 1).await {
                        warn!("publish error {}", e);
                    }
                    mqttclient.disconnect().await;
                    CHANNEL.send([5, 0, 0, 0]).await;
                }
                _ => {}
            }
            // 120s
            Timer::after(Duration::from_secs(((60 - seconds) + 60) as u64)).await;
        }
    }
}

async fn get_ntp_time(stack: &'static embassy_net::Stack<Device<'static>>) -> u32 {
    let mut ntp_client = NTPClient::new();
    if let Ok(ntp_time) = ntp_client.get_ntp_time(stack).await {
        return ntp_time;
    }
    return 0;
}

fn day_of_week_from_u8(v: u8) -> DayOfWeek {
    match v {
        0 => DayOfWeek::Sunday,
        1 => DayOfWeek::Monday,
        2 => DayOfWeek::Tuesday,
        3 => DayOfWeek::Wednesday,
        4 => DayOfWeek::Thursday,
        5 => DayOfWeek::Friday,
        6 => DayOfWeek::Saturday,
        _ => return DayOfWeek::Sunday,
    }
}

fn get_datetime_from_seconds(seconds: u64) -> DateTime {
    let c = Calendar::create();
    let dt = c.from_unixtime(seconds * 1000);
    let dow: u8 = (((seconds / 86400) + 4) % 7) as u8;

    DateTime {
        year: dt.year,
        month: dt.month,
        day: dt.day,
        day_of_week: day_of_week_from_u8(dow),
        hour: dt.hour,
        minute: dt.minute,
        second: dt.second,
    }
}

fn get_datetime_formatted(dt: DateTime) -> String<TIME_DATE_STRING_LENGTH> {
    let mut date: String<TIME_DATE_STRING_LENGTH> = String::new();
    write!(date, "{:4}/{:02}/{:02} {:02}:{:02}", dt.year, dt.month, dt.day, dt.hour,
           dt.minute).unwrap();
    date
}

fn get_naivedatetime_formatted(dt: NaiveDateTime) -> String<TIME_DATE_STRING_LENGTH> {
    let mut date: String<TIME_DATE_STRING_LENGTH> = String::new();
    write!(date, "{:4}/{:02}/{:02} {:02}:{:02}", dt.year(), dt.month(), dt.day(), dt.hour(),
           dt.minute()).unwrap();
    date
}

fn calc_brightness_pwm(brightness: u8) -> f32 {
    let gamma = 2.8;
    F32Ext::powf(brightness as f32 / 255.0, gamma) * 65535.0 + 0.5
}

#[main]
async fn main(spawner: Spawner) {
    let p: Peripherals = embassy_rp::init(Default::default());
    let mut buf: String<1024> = String::new();

    let rtc: Rtc<RTC> = Rtc::new(p.RTC);
    let rtc_static = make_static!(rtc);


    let mut character_styles = CharacterStyles {
        default_character_style: None,
        small_character_style: None,
        text_style_center_aligned: None,
        fill_style: None,
        seven_segment_style: None,
    };
    character_styles.init();

    // display
    let mosi = p.PIN_11;
    let clk = p.PIN_10;

    let mut spi_config = spi::Config::default();
    spi_config.frequency = DISPLAY_FREQ;
    spi_config.phase = spi::Phase::CaptureOnSecondTransition;
    spi_config.polarity = spi::Polarity::IdleHigh;

    let display_spi =
        spi::Spi::new_blocking_txonly(p.SPI1, clk, mosi, spi_config.clone());
    let spi_bus: blocking_mutex::Mutex<blocking_mutex::raw::NoopRawMutex, _> = blocking_mutex::Mutex::new(RefCell::new(display_spi));

    let rst = p.PIN_12;
    let display_cs = p.PIN_9;
    let dcx = p.PIN_8;
    let busy = p.PIN_13;

    let dcx = Output::new(dcx, Level::Low);
    let rst = Output::new(rst, Level::Low);
    let busy = Input::new(busy, Pull::Up);

    let display_cs = Output::new(display_cs, Level::High);
    let mut display_spi = blocking::spi::SpiDevice::new(
        &spi_bus,
        display_cs,
    );

    let mut epd = Epd2in9::new(&mut display_spi, busy, dcx, rst, &mut Delay, None).unwrap();
    let mut display: Display2in9 = Display2in9::default();
    display.set_rotation(DisplayRotation::Rotate90);

    epd.set_background_color(DEFAULT_BACKGROUND_COLOR);
    epd.clear_frame(&mut display_spi, &mut Delay).unwrap();
    epd.display_frame(&mut display_spi, &mut Delay).unwrap();

    // Generate random seed.
    let mut rng = RoscRng;
    let seed = rng.next_u64();

    // wifi
    let fw = include_bytes!("../firmware/43439A0.bin");
    let clm = include_bytes!("../firmware/43439A0_clm.bin");
    // To make flashing faster for development, you may want to flash the firmwares independently
    // at hardcoded addresses, instead of baking them into the program with `include_bytes!`:
    //     probe-rs download 43439A0.bin --format bin --chip RP2040 --base-address 0x10100000
    //     probe-rs download 43439A0_clm.bin --format bin --chip RP2040 --base-address 0x10140000
    //let fw = unsafe { core::slice::from_raw_parts(0x10100000 as *const u8, 230321) };
    //let clm = unsafe { core::slice::from_raw_parts(0x10140000 as *const u8, 4752) };

    let pwr = Output::new(p.PIN_23, Level::Low);
    let cs = Output::new(p.PIN_25, Level::High);
    let mut pio = Pio::new(p.PIO0, Irqs);
    let spi = PioSpi::new(&mut pio.common, pio.sm0, pio.irq0, cs, p.PIN_24, p.PIN_29, p.DMA_CH0);

    static STATE: StaticCell<cyw43::State> = StaticCell::new();
    let state = STATE.init(cyw43::State::new());
    let (_net_device, mut control, runner) = cyw43::new(state, pwr, spi, fw).await;
    unwrap!(spawner.spawn(wifi_task(runner)));

    control.init(clm).await;
    control.set_power_management(cyw43::PowerManagementMode::PowerSave).await;

    // let wifi_config = embassy_net::Config::dhcpv4(Default::default());
    let wifi_config = embassy_net::Config::ipv4_static(embassy_net::StaticConfigV4 {
        address: Ipv4Cidr::new(Ipv4Address::new(192, 168, 1, 111), 24),
        gateway: Some(Ipv4Address::new(192, 168, 1, 1)),
        dns_servers: Default::default(),
    });
    // Init network stack
    static STACK: StaticCell<Stack<cyw43::NetDriver<'static>>> = StaticCell::new();
    static RESOURCES: StaticCell<embassy_net::StackResources<3>> = StaticCell::new();

    let stack = &*STACK.init(Stack::new(
        _net_device,
        wifi_config,
        RESOURCES.init(embassy_net::StackResources::new()),
        seed,
    ));

    unwrap!(spawner.spawn(net_task(stack)));

    display
        .bounding_box()
        .into_styled(character_styles.fill_style())
        .draw(&mut display)
        .unwrap();

    Text::with_text_style(
        "Waiting for network ...",
        Point::new(10, 10),
        character_styles.default_character_style(),
        character_styles.text_style_center_aligned(),
    )
        .draw(&mut display)
        .unwrap();
    epd.update_and_display_frame(&mut display_spi, display.buffer(), &mut Delay).unwrap();

    loop {
        match control.join_wpa2(WIFI_NETWORK, WIFI_PASSWORD).await {
            Ok(_) => break,
            Err(err) => {
                info!("join failed with status={}", err.status);
            }
        }
    }

    // info!("waiting for DHCP...");
    // while !stack.is_config_up() {
    //     Timer::after_millis(100).await;
    // }
    // info!("IP address: {:?}", stack.config_v4().unwrap().address.address());

    let ntp_time: u64 = get_ntp_time(stack).await.into();
    if ntp_time != 0 {
        let mut now = get_datetime_from_seconds(ntp_time);
        info!("UTC Now: {}/{:02}/{:02} {:02}:{:02}:{:02}",
            now.year, now.month, now.day, now.hour, now.minute, now.second,);

        let mut ntp_time_local = ntp_time + UTC_TZ_DIFF;

        now = get_datetime_from_seconds(ntp_time_local);
        info!("TZ Now: {}/{:02}/{:02} {:02}:{:02}:{:02}",
            now.year, now.month, now.day, now.hour, now.minute, now.second,);

        // check if we are in EU DST time or not
        let is_month_in_dst = now.month >= 3 && now.month <= 10;
        if is_month_in_dst {
            let year = now.year;
            let x = 33 + year + year / 4 - year / 100 + year / 400; //common part of Keith for Mar and Oct
            let last_sun_march = 31 - x % 7;
            let last_sun_oct = 31 - (4 + x) % 7;

            let mut is_day_in_dst = false;
            match now.month {
                3 => {
                    is_day_in_dst = (now.day as u16) >= last_sun_march;
                }
                10 => {
                    is_day_in_dst = (now.day as u16) < last_sun_oct;
                }
                _ => {
                    is_day_in_dst = true;
                }
            }
            if is_day_in_dst {
                ntp_time_local = ntp_time_local + DST_DIFF;
                now = get_datetime_from_seconds(ntp_time_local);

                info!("TZ DST Now: {}/{:02}/{:02} {:02}:{:02}:{:02}",
            now.year, now.month, now.day, now.hour, now.minute, now.second,);
            }
        }

        rtc_static.set_datetime(now).unwrap();
    } else {
        error!("Failed to get ntp time");
    }


    // unwrap!(spawner.spawn(handle_mqtt_subscribe(stack)));
    unwrap!(spawner.spawn(handle_mqtt_subscribe_periodic(stack, rtc_static)));
    // unwrap!(spawner.spawn(handle_mqtt_publish(stack, rtc_static)));

    let i2c0 = i2c::I2c::new_blocking(p.I2C0, p.PIN_5, p.PIN_4, i2c::Config::default());
    let i2c0_bus = blocking_mutex::Mutex::<blocking_mutex::raw::ThreadModeRawMutex, _>::new(RefCell::new(i2c0));
    let i2c0_bus_static = make_static!(i2c0_bus);

    let i2c0_dev1 = blocking::i2c::I2cDevice::new(i2c0_bus_static);
    let i2c0_dev2 = blocking::i2c::I2cDevice::new(i2c0_bus_static);
    let i2c0_dev3 = blocking::i2c::I2cDevice::new(i2c0_bus_static);

    unwrap!(spawner.spawn(handle_bme68x_sensor(i2c0_dev1, stack, rtc_static)));
    unwrap!(spawner.spawn(handle_tp_sensor(i2c0_dev3, p.PIN_17.degrade(), p.PIN_16.degrade(), display.size(), display.rotation())));

    epd.update_old_frame(&mut display_spi, &display.buffer(), &mut Delay).unwrap();
    display
        .bounding_box()
        .into_styled(character_styles.fill_style())
        .draw(&mut display)
        .unwrap();
    epd.update_and_display_new_frame(&mut display_spi, display.buffer(), &mut Delay).unwrap();

    loop {
        let buffer: [u16; 4] = CHANNEL.receive().await;
        info!("main loop {}", buffer);
        match buffer[0] {
            3 => {
                info!("x = {} y = {} z = {}", buffer[1], buffer[2], buffer[3]);
                continue;
            }
            _ => {}
        }

        epd.wake_up(&mut display_spi, &mut Delay).unwrap();
        epd.update_old_frame(&mut display_spi, &display.buffer(), &mut Delay).unwrap();

        display
            .bounding_box()
            .into_styled(character_styles.fill_style())
            .draw(&mut display)
            .unwrap();

        let sensor_data = BME688_DATA.lock().await.get_mut().clone();
        // info!("{}", sensor_data);
        if sensor_data.valid {
            // buf.clear();
            // write!(&mut buf, "Indoor").unwrap();
            //
            // Text::with_text_style(
            //     buf.as_str(),
            //     Point::new(10, 10),
            //     character_styles.default_character_style(),
            //     character_styles.text_style_center_aligned(),
            // )
            //     .draw(&mut display)
            //     .unwrap();

            buf.clear();
            write!(&mut buf, "{:width$.1}", sensor_data.temperature, width = 4).unwrap();

            Text::with_text_style(
                buf.as_str(),
                Point::new(20, 50),
                character_styles.seven_segment_style(),
                character_styles.text_style_center_aligned(),
            )
                .draw(&mut display)
                .unwrap();

            buf.clear();
            write!(&mut buf, "{humidity:.*}% {pressure:.*}hPa", 1, 1,
                   humidity = sensor_data.humidity as i32,
                   pressure = sensor_data.pressure as i32).unwrap();

            Text::with_text_style(
                buf.as_str(),
                Point::new(20, 105),
                character_styles.default_character_style(),
                character_styles.text_style_center_aligned(),
            )
                .draw(&mut display)
                .unwrap();

            buf.clear();
            write!(&mut buf, "{}", get_datetime_formatted(get_datetime_from_seconds(sensor_data.time.try_into().unwrap()))).unwrap();

            Text::with_text_style(
                buf.as_str(),
                Point::new(20, 120),
                character_styles.small_character_style(),
                character_styles.text_style_center_aligned(),
            )
                .draw(&mut display)
                .unwrap();
        }

        unsafe {
            if (MQTT_SUBSCRIBE_DATA.msg.len() != 0) {
                match serde_json_core::from_str(MQTT_SUBSCRIBE_DATA.msg.as_str()) {
                    Ok(res) => {
                        let sensor_data: SensorData = res.0;
                        // buf.clear();
                        // write!(&mut buf, "Outdoor").unwrap();
                        // Text::with_text_style(
                        //     buf.as_str(),
                        //     Point::new(10, 70),
                        //     character_styles.default_character_style(),
                        //     character_styles.text_style_center_aligned(),
                        // )
                        //     .draw(&mut display)
                        //     .unwrap();

                        buf.clear();
                        write!(&mut buf, "{:width$.1}", sensor_data.temperature, width = 4).unwrap();
                        Text::with_text_style(
                            buf.as_str(),
                            Point::new(160, 50),
                            character_styles.seven_segment_style(),
                            character_styles.text_style_center_aligned(),
                        )
                            .draw(&mut display)
                            .unwrap();

                        buf.clear();
                        write!(&mut buf, "{humidity:.*}% {pressure:.*}hPa", 1, 1,
                               humidity = sensor_data.humidity as i32,
                               pressure = sensor_data.pressure as i32).unwrap();

                        Text::with_text_style(
                            buf.as_str(),
                            Point::new(170, 105),
                            character_styles.default_character_style(),
                            character_styles.text_style_center_aligned(),
                        )
                            .draw(&mut display)
                            .unwrap();

                        buf.clear();
                        write!(&mut buf, "{}", get_datetime_formatted(get_datetime_from_seconds(sensor_data.time.try_into().unwrap()))).unwrap();

                        // write!(&mut buf, "Time {}", sensor_data.time).unwrap();
                        Text::with_text_style(
                            buf.as_str(),
                            Point::new(170, 120),
                            character_styles.small_character_style(),
                            character_styles.text_style_center_aligned(),
                        )
                            .draw(&mut display)
                            .unwrap();
                    }
                    Err(err) => {
                        info!("mqtt_subscribe from_str error");
                    }
                }
            }
        }

        epd.update_and_display_new_frame(&mut display_spi, display.buffer(), &mut Delay).unwrap();
        epd.sleep(&mut display_spi, &mut Delay).unwrap();
    }
    control.leave().await;
}
