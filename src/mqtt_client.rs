use core::future::Future;
use core::str::FromStr;

use byteorder::{BigEndian, ByteOrder};
use defmt::{info, warn};
use embassy_net::Stack;
use embassy_net::tcp::TcpSocket;
use embassy_net_wiznet::Device;
use heapless::String;

use crate::from_ascii;

#[derive(Debug, Clone)]
pub struct MqttSubscribeData {
    pub topic: String<128>,
    pub msg: String<256>,
}

impl core::fmt::Display for MqttSubscribeData {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "{} {}", self.topic.as_str(), self.msg.as_str())
    }
}

impl defmt::Format for MqttSubscribeData {
    fn format(&self, f: defmt::Formatter) {
        defmt::write!(f, "{} {}", self.topic.as_str(), self.msg.as_str());
    }
}


pub(crate) struct MQTTClient<'a> {
    socket: TcpSocket<'a>,
    pid: u16,
}

impl<'a> MQTTClient<'a> {
    pub fn new(stack: &'a Stack<Device>, rx_buf: &'a mut [u8], tx_buf: &'a mut [u8]) -> Self {
        Self { socket: TcpSocket::new(stack, rx_buf, tx_buf), pid: 0 }
    }

    pub async fn connect(&mut self, client_id: &str, server: &str, port: u16, user: &str, password: &str) -> Result<usize, &'a str> {
        //self.socket.set_timeout(Some(Duration::from_secs(10)));
        let host_addr = embassy_net::Ipv4Address::from_str(server).unwrap();
        if let Err(_e) = self.socket.connect((host_addr, port)).await {
            return Err("connect error");
        }
        info!("Connected to {:?}", self.socket.remote_endpoint());

        let mut premsg = [0u8; 5];
        premsg[0] = 0x10;

        let mut msg = [0u8; 10];
        msg[1] = 0x04;
        msg[2] = 0x4d;
        msg[3] = 0x51;
        msg[4] = 0x54;
        msg[5] = 0x54;
        msg[6] = 0x04;

        let mut size: u16 = (10 + 2 + client_id.len()) as u16;
        // clean start - bit 2
        msg[7] = 2;

        size += (2 + user.len() + 2 + password.len()) as u16;
        // bit 6 and 7
        msg[7] |= 0xC0;

        let mut i = 1;
        while size > 0x7F {
            premsg[i] = ((size & 0x7F) | 0x80) as u8;
            size >>= 7;
            i += 1;
        }
        premsg[i] = size as u8;

        // info!("premsg_cut = {} msg = {}", &premsg[..i + 1], msg);

        if let Err(_e) = self.socket.write(&premsg[..i + 1]).await {
            return Err("write error");
        }
        if let Err(_e) = self.socket.write(&msg).await {
            return Err("write error");
        }
        self.send_str(client_id.as_bytes()).await?;
        self.send_str(user.as_bytes()).await?;
        self.send_str(password.as_bytes()).await?;

        let mut buf: [u8; 4] = [0u8; 4];
        if let Err(_e) = self.socket.read(&mut buf).await {
            return Err("read error");
        }
        // info!("read {}", buf);
        if buf[0] == 0x20 && buf[1] == 0x02 {
            if buf[3] != 0 {
                warn!("connect error");
            }
        } else {
            warn!("connect error");
        }
        Ok(0)
    }

    async fn send_str(&mut self, s: &[u8]) -> Result<usize, &'a str> {
        let mut buf: [u8; 2] = [0u8; 2];
        BigEndian::write_u16(&mut buf, s.len() as u16);
        if let Err(_e) = self.socket.write(&buf).await {
            return Err("write error");
        }
        if let Err(_e) = self.socket.write(s).await {
            return Err("write error");
        }
        Ok(0)
    }

    pub async fn ping(&mut self) {
        if let Err(e) = self.socket.write(b"\xc0\0").await {
            warn!("write error: {:?}", e);
        }
    }
    pub async fn disconnect(&mut self) {
        if let Err(e) = self.socket.write(b"\xe0\0").await {
            warn!("write error: {:?}", e);
        }
        self.socket.close();
    }

    pub async fn publish(&mut self, topic: &str, payload: &str, retain: bool, qos: u8) -> Result<usize, &'a str> {
        let mut msg = [0u8; 5];
        msg[0] = 0x30;
        msg[0] |= qos << 1;
        if retain {
            msg[0] |= retain as u8;
        }

        let mut size: u32 = (2 + topic.len() + payload.len()) as u32;
        if qos > 0 {
            size += 2;
        }

        if size > 2097152 {
            return Err("payload too long");
        }

        let orig_size = size;
        let mut i = 1;
        while size > 0x7F {
            msg[i] = ((size & 0x7F) | 0x80) as u8;
            size >>= 7;
            i += 1;
        }
        msg[i] = size as u8;

        info!("publish topic = {} msg = {}", topic, &msg[..i + 1]);

        if let Err(_e) = self.socket.write(&msg[..i + 1]).await {
            return Err("write error");
        }
        self.send_str(topic.as_bytes()).await?;

        if qos > 0 {
            // qos add message id
            self.pid += 1;
            let mut buf: [u8; 2] = [0u8; 2];
            BigEndian::write_u16(&mut buf, self.pid);
            if let Err(_e) = self.socket.write(&buf).await {
                return Err("write error");
            }
        }

        if let Err(_e) = self.socket.write(payload.as_bytes()).await {
            return Err("write error");
        }

        if qos == 1 {
            loop {
                info!("publish wait pid = {}", self.pid);
                match self.wait_msg().await {
                    Ok((op, msg)) => {
                        if op == 0x40 {
                            // info!("publish PUBACK op = {} {:08b}", op, op);
                            let mut qos_buf: [u8; 3] = [0u8; 3];
                            if let Err(_e) = self.socket.read(&mut qos_buf).await {
                                return Err("read error");
                            }
                            // info!("publish PUBACK pkt = {:08b}", qos_buf[0]);
                            // info!("publish PUBACK pkt = {:08b}", qos_buf[1]);
                            // info!("publish PUBACK pkt = {:08b}", qos_buf[2]);

                            let size = qos_buf[0];
                            if size == 2 {
                                let rcv_pid = BigEndian::read_u16(&qos_buf[1..3]);
                                info!("publish wait rcv_pid = {}", rcv_pid);
                                if self.pid == rcv_pid {
                                    break;
                                }
                                // The publisher will continue sending this message if it does not receive a PUBACK packet.
                            }
                        } else {
                            //TODO The publisher will continue sending this message if it does not receive a PUBACK packet.
                        }
                    }
                    Err(e) => {
                        warn!("wait_msg {}", e);
                    }
                }
            }
        }
        Ok(0)
    }
    async fn wait_msg(&mut self) -> Result<(usize, String<256>), &'a str>

    {
        let mut buf: [u8; 1] = [0u8; 1];
        if let Err(_e) = self.socket.read(&mut buf).await {
            return Ok((0, String::from("")));
        }
        let mut op = buf[0];
        // info!("wait_msg op = {} {:08b}", op, op);
        if op == 0x0 {
            return Ok((0, String::from("")));
        }
        if op == 0xD0 { //PINGRESP
            if let Err(_e) = self.socket.read(&mut buf).await {
                return Err("read error");
            }
            return Ok((op as usize, String::from("")));
        }
        if op & 0xF0 != 0x30 {
            return Ok((op as usize, String::from("")));
        }

        let mut sz_result = self.recv_len().await;
        if sz_result.is_err() {
            return Err(sz_result.err().unwrap());
        }
        let mut sz: u32 = sz_result.unwrap();
        // info!("wait_msg sz = {}", sz);

        let mut topic_len_buf: [u8; 2] = [0u8; 2];
        if let Err(_e) = self.socket.read(&mut topic_len_buf).await {
            return Err("read error");
        }

        let topic_len: u16 = BigEndian::read_u16(&topic_len_buf);
        // info!("wait_msg topic_len = {}", topic_len);

        let mut topic: [u8; 1024] = [0u8; 1024];
        let mut topic_buf: [u8; 1] = [0u8; 1];
        for i in 0..topic_len {
            if let Err(_e) = self.socket.read(&mut topic_buf).await {
                return Err("read error");
            }
            topic[i as usize] = topic_buf[0];
        }

        let topic_res = from_ascii(&topic[0..topic_len as usize]);
        if topic_res.is_err() {
            return Err(topic_res.err().unwrap());
        }
        let topic_str = topic_res.unwrap();
        info!("wait_msg topic = {}", topic_str);

        sz -= topic_len as u32 + 2;

        let mut pid: u16 = 0;
        if op & 6 != 0 {
            let mut pid_buf: [u8; 2] = [0u8; 2];
            if let Err(_e) = self.socket.read(&mut pid_buf).await {
                return Err("read error");
            }
            pid = BigEndian::read_u16(&pid_buf);
            // info!("wait_msg pid = {}", pid);
            sz -= 2;
        }

        let mut msg: [u8; 1024] = [0u8; 1024];
        let mut msg_buf: [u8; 1] = [0u8; 1];
        for i in 0..sz {
            if let Err(_e) = self.socket.read(&mut msg_buf).await {
                return Err("read error");
            }
            msg[i as usize] = msg_buf[0];
        }
        let msg_res = from_ascii(&msg[0..256.min(sz as usize)]);
        if msg_res.is_err() {
            return Err(topic_res.err().unwrap());
        }
        let msg = msg_res.unwrap();
        info!("wait_msg msg = {}", msg);


        if op & 6 == 2 {
            let mut pkt = [0u8; 4];
            pkt[0] = 0x40;
            pkt[1] = 0x02;
            BigEndian::write_u16(&mut pkt[2..4], pid);
            // info!("wait_msg send pid = {}", pid);

            if let Err(_e) = self.socket.write(&pkt).await {
                return Err("write error");
            }
        } else if op & 6 == 4 {
            return Err("wait_msg error");
        }
        return Ok((op as usize, String::from(msg)));
    }

    async fn recv_len(&mut self) -> Result<u32, &'a str> {
        let mut n: u32 = 0;
        let mut sh = 0;
        let mut buf: [u8; 1] = [0u8; 1];

        loop {
            if let Err(_e) = self.socket.read(&mut buf).await {
                return Err("read error");
            }
            let b = buf[0];
            let bn: u32 = (b & 0x7F).into();
            n |= bn << sh;
            if b & 0x80 == 0 {
                return Ok(n);
            }
            sh += 7;
        }
        Err("recv_len")
    }

    pub async fn subscribe<F, Fut>(&mut self, topic: &str, qos: u8, cb: F) -> Result<u32, &'a str>
        where F: Fn(MqttSubscribeData) -> Fut,
              Fut: Future<Output=bool>
    {
        self.pid += 1;
        let mut pkt = [0u8; 4];
        pkt[0] = 0x82;
        pkt[1] = (2 + 2 + topic.len() + 1) as u8;
        BigEndian::write_u16(&mut pkt[2..4], self.pid);

        if let Err(_e) = self.socket.write(&pkt).await {
            return Err("write error");
        }
        self.send_str(topic.as_bytes()).await?;
        let mut buf = [0u8; 1];
        buf[0] = qos;
        if let Err(_e) = self.socket.write(&buf).await {
            return Err("write error");
        }

        loop {
            match self.wait_msg().await {
                Ok((op, msg)) => {
                    if op == 0xD0 {
                        info!("subscribe PINGRESP op = {} {:08b}", op, op);
                    } else if op == 0x90 {
                        // info!("subscribe SUBACK op = {} {:08b}", op, op);
                        pkt = [0u8; 4];

                        if let Err(_e) = self.socket.read(&mut pkt).await {
                            return Err("read error");
                        }
                        // info!("subscribe SUBACK pkt = {:08b}", pkt[0]);
                        // info!("subscribe SUBACK pkt = {:08b}", pkt[1]);
                        // info!("subscribe SUBACK pkt = {:08b}", pkt[2]);
                        // info!("subscribe SUBACK pkt = {:08b}", pkt[3]);

                        info!("subscribe SUBACK qos level requested {} - granted {}", qos, pkt[3]);
                    } else {
                        let data = crate::mqtt_client::MqttSubscribeData {
                            topic: String::from(topic),
                            msg,
                        };
                        cb(data).await;
                        self.ping().await;
                    }
                }
                Err(e) => {
                    warn!("wait_msg {}", e);
                }
            }
        }
        Ok(0)
    }

    pub async fn subscribe_oneshot<F, Fut>(&mut self, topic: &str, qos: u8, cb: F) -> Result<u32, &'a str>
    where F: Fn(MqttSubscribeData) -> Fut,
          Fut: Future<Output=bool>
    {
        self.pid += 1;
        let mut pkt = [0u8; 4];
        pkt[0] = 0x82;
        pkt[1] = (2 + 2 + topic.len() + 1) as u8;
        BigEndian::write_u16(&mut pkt[2..4], self.pid);

        if let Err(_e) = self.socket.write(&pkt).await {
            return Err("write error");
        }
        self.send_str(topic.as_bytes()).await?;
        let mut buf = [0u8; 1];
        buf[0] = qos;
        if let Err(_e) = self.socket.write(&buf).await {
            return Err("write error");
        }

        loop {
            match self.wait_msg().await {
                Ok((op, msg)) => {
                    if op == 0xD0 {
                        info!("subscribe PINGRESP op = {} {:08b}", op, op);
                    } else if op == 0x90 {
                        // info!("subscribe SUBACK op = {} {:08b}", op, op);
                        pkt = [0u8; 4];

                        if let Err(_e) = self.socket.read(&mut pkt).await {
                            return Err("read error");
                        }
                        // info!("subscribe SUBACK pkt = {:08b}", pkt[0]);
                        // info!("subscribe SUBACK pkt = {:08b}", pkt[1]);
                        // info!("subscribe SUBACK pkt = {:08b}", pkt[2]);
                        // info!("subscribe SUBACK pkt = {:08b}", pkt[3]);

                        info!("subscribe SUBACK qos level requested {} - granted {}", qos, pkt[3]);
                    } else {
                        let data = crate::mqtt_client::MqttSubscribeData {
                            topic: String::from(topic),
                            msg,
                        };
                        cb(data).await;
                        self.disconnect().await;
                        break;
                    }
                }
                Err(e) => {
                    warn!("wait_msg {}", e);
                }
            }
        }
        Ok(0)
    }
}

impl<'a> Drop for MQTTClient<'a> {
    fn drop(&mut self) {
        info!("Drop MQTTClient")
    }
}