use core::str::FromStr;
use byteorder::{BigEndian, ByteOrder};
use defmt::info;
use embassy_net::{Ipv4Address, Stack};
use embassy_net::udp::{PacketMetadata, UdpSocket};
use embassy_net_wiznet::Device;

const NTP_DELTA: u32 = 2208988800;

pub(crate) struct NTPClient {
    rx_buffer: [u8; 4096],
    tx_buffer: [u8; 4096],
    rx_meta: [PacketMetadata; 16],
    tx_meta: [PacketMetadata; 16],
}

impl<'a> NTPClient {
    pub fn new() -> Self {
        Self {
            rx_buffer: [0u8; 4096],
            tx_buffer: [0u8; 4096],
            rx_meta: [PacketMetadata::EMPTY; 16],
            tx_meta: [PacketMetadata::EMPTY; 16],
        }
    }

    pub async fn get_ntp_time(&mut self, stack: &'a Stack<Device<'a>>) -> Result<u32, &'a str> {
        let mut buf = [0; 48];
        let host_addr = Ipv4Address::from_str("162.159.200.1").unwrap();

        let mut socket = UdpSocket::new(stack, self.rx_meta.as_mut(),
                                        self.rx_buffer.as_mut(), self.tx_meta.as_mut(),
                                        self.tx_buffer.as_mut());

        if let Err(_e) = socket.bind(123) {
            return Err("bind failed");
        }

        buf[0] = 0x1B;
        if let Err(_e) = socket.send_to(&buf, (host_addr, 123)).await {
            return Err("send_to error");
        }

        if let Err(_e) = socket.recv_from(&mut buf).await {
            return Err("recv_from error");
        }

        socket.close();

        let mut ntp_time = BigEndian::read_u32(&buf[40..44]);
        ntp_time -= NTP_DELTA;
        Ok(ntp_time)
    }
}

impl<'a> Drop for NTPClient {
    fn drop(&mut self) {
        info!("Drop NTPClient")
    }
}