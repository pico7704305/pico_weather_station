use core::fmt::Debug;

use byteorder::{BigEndian, ByteOrder};
use cortex_m::prelude::{_embedded_hal_blocking_i2c_Read, _embedded_hal_blocking_i2c_Write};
use embedded_hal::i2c::I2c;

pub struct TpSensor<I2C> {
    /// Underlying I2C device
    i2c: I2C,
}

const TP_ADDR: u8 = 0x48;

impl<I2C: I2c> TpSensor<I2C> {
    pub fn new(i2c: I2C) -> Self {
        TpSensor {
            i2c,
        }
    }
    fn i2c_writebyte(&mut self, reg: u16, value: u8) -> Result<(), I2C::Error> {
        let mut wbuf: [u8; 3] = [0u8; 3];
        BigEndian::write_u16(&mut wbuf, reg);
        self.i2c.write(TP_ADDR, &mut wbuf)
    }

    fn i2c_readbyte(&mut self, reg: u16) -> Result<[u8; 64], I2C::Error> {
        let mut wbuf: [u8; 2] = [0u8; 2];
        BigEndian::write_u16(&mut wbuf, reg);
        self.i2c.write(TP_ADDR, &mut wbuf)?;

        let mut rbuf: [u8; 64] = [0u8; 64];
        self.i2c.read(TP_ADDR, &mut rbuf)?;
        return Ok(rbuf);
    }

    pub fn touch(&mut self) -> Result<(u16, u16, u16), I2C::Error> {
        let mask: u8 = 0x00;

        if let Ok(rbuf) = self.i2c_readbyte(0x1001) {
            if rbuf[0] == 0x00 {
                self.i2c_writebyte(0x1001, mask)?;
            } else {
                let tp_count = rbuf[0];
                if tp_count > 5 || tp_count < 1 {
                    self.i2c_writebyte(0x1001, mask)?;
                } else {
                    if let Ok(rbuf) = self.i2c_readbyte(0x1002) {
                        if let Ok(()) = self.i2c_writebyte(0x1001, mask) {
                            //for finger in 0..tp_count {
                            let i = 0; //finger as usize;
                            let x1 = rbuf[4 + 7 * i] as u16;
                            let x = 127 - ((x1 << 8) + rbuf[3 + 7 * i] as u16);
                            let y1 = rbuf[2 + 7 * i] as u16;
                            let y = ((y1 << 8) + rbuf[1 + 7 * i] as u16);
                            let p = rbuf[5 + 7 * i] as u16;

                            return Ok((x, y, p));
                            //}
                        }
                    }
                }
            }
        }
        return Ok((0, 0, 0));
    }
}